package uz.pdp.appcompany.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class WorkerDto {
    @NotNull(message = "PhoneNumber is not to be null")
    private String phoneNumber;

    @NotNull(message = "Address is not to be null")
    private Integer addressId;

    @NotNull(message = "Department is not to be null")
    private Integer departmentId;
}
