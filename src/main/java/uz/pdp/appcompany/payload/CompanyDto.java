package uz.pdp.appcompany.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CompanyDto {
    @NotNull(message = "CorpName is not to be null")
    private String corpName;

    @NotNull(message = "DirectorName is not to be null")
    private String directorName;

    @NotNull(message = "Address is not to be null")
    private Integer addressId;
}
