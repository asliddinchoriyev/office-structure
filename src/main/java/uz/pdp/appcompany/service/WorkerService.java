package uz.pdp.appcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.appcompany.entity.Address;
import uz.pdp.appcompany.entity.Company;
import uz.pdp.appcompany.entity.Department;
import uz.pdp.appcompany.entity.Worker;
import uz.pdp.appcompany.payload.ApiResponse;
import uz.pdp.appcompany.payload.DepartmentDto;
import uz.pdp.appcompany.payload.WorkerDto;
import uz.pdp.appcompany.repository.AddressRepository;
import uz.pdp.appcompany.repository.CompanyRepository;
import uz.pdp.appcompany.repository.DepartmentRepository;
import uz.pdp.appcompany.repository.WorkerRepository;

import java.util.List;
import java.util.Optional;

@Service
public class WorkerService {
    @Autowired
    DepartmentRepository departmentRepository;
    @Autowired
    AddressRepository addressRepository;
    @Autowired
    WorkerRepository workerRepository;

    public ApiResponse add(WorkerDto workerDto) {

        boolean byPhoneNumber = workerRepository.existsByPhoneNumber(workerDto.getPhoneNumber());
        if (byPhoneNumber)
            return new ApiResponse("This worker already added", false);

        Optional<Address> optionalAddress = addressRepository.findById(workerDto.getAddressId());
        if (optionalAddress.isEmpty())
            return new ApiResponse("This address not found", false);

        Optional<Department> optionalDepartment = departmentRepository.findById(workerDto.getDepartmentId());
        if (optionalDepartment.isEmpty())
            return new ApiResponse("This department not found", false);

        Worker worker = new Worker();
        worker.setPhoneNumber(workerDto.getPhoneNumber());
        worker.setAddress(optionalAddress.get());
        worker.setDepartment(optionalDepartment.get());

        workerRepository.save(worker);

        return new ApiResponse("The worker added", true);
    }

    public List<Worker> getAll() {
        List<Worker> all = workerRepository.findAll();

        return all;
    }

    public Worker getById(Integer id) {
        Optional<Worker> optionalWorker = workerRepository.findById(id);

        return optionalWorker.orElse(null);
    }

    public ApiResponse edit(WorkerDto workerDto, Integer id) {

        Optional<Worker> optionalWorker = workerRepository.findById(id);

        if (optionalWorker.isEmpty())
            return new ApiResponse("The worker not found", false);

        Optional<Address> optionalAddress = addressRepository.findById(workerDto.getAddressId());
        if (optionalAddress.isEmpty())
            return new ApiResponse("This address not found", false);

        Optional<Department> optionalDepartment = departmentRepository.findById(workerDto.getDepartmentId());
        if (optionalDepartment.isEmpty())
            return new ApiResponse("This department not found", false);

        boolean exists = workerRepository.existsByPhoneNumberAndIdNot(workerDto.getPhoneNumber(), id);
        if (exists)
            return new ApiResponse("This worker already added", false);


        Worker worker = new Worker();
        worker.setPhoneNumber(workerDto.getPhoneNumber());
        worker.setAddress(optionalAddress.get());
        worker.setDepartment(optionalDepartment.get());

        workerRepository.save(worker);

        return new ApiResponse("The worker edited", true);
    }

    public ApiResponse delete(Integer id) {

        Optional<Department> optionalDepartment = departmentRepository.findById(id);

        if (optionalDepartment.isEmpty())
            return new ApiResponse("The department not found", false);

        departmentRepository.deleteById(id);
        return new ApiResponse("The department deleted", true);
    }
}
