package uz.pdp.appcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.appcompany.entity.Address;
import uz.pdp.appcompany.entity.Company;
import uz.pdp.appcompany.payload.ApiResponse;
import uz.pdp.appcompany.payload.CompanyDto;
import uz.pdp.appcompany.repository.AddressRepository;
import uz.pdp.appcompany.repository.CompanyRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {
    @Autowired
    AddressRepository addressRepository;
    @Autowired
    CompanyRepository companyRepository;

    public ApiResponse add(CompanyDto companyDto) {
        boolean existsByCorpName = companyRepository.existsByCorpName(companyDto.getCorpName());
        if (existsByCorpName)
            return new ApiResponse("This company already added", false);

        Company company = new Company();

        Optional<Address> optionalAddress = addressRepository.findById(companyDto.getAddressId());
        if (optionalAddress.isEmpty())
            return new ApiResponse("This address not found", false);

        Address address = optionalAddress.get();

        company.setCorpName(companyDto.getCorpName());
        company.setDirectorName(companyDto.getDirectorName());
        company.setAddress(address);

        companyRepository.save(company);

        return new ApiResponse("The company added", true);
    }

    public List<Company> getAll() {
        List<Company> all = companyRepository.findAll();

        return all;
    }

    public Company getById(Integer id) {
        Optional<Company> optionalCompany = companyRepository.findById(id);

        return optionalCompany.orElse(null);
    }

    public ApiResponse edit(CompanyDto companyDto, Integer id) {

        Optional<Company> optionalCompany = companyRepository.findById(id);

        if (optionalCompany.isEmpty())
            return new ApiResponse("The company not found", false);

        boolean nameAndIdNot = companyRepository.existsByCorpNameAndIdNot(companyDto.getCorpName(), id);
        if (nameAndIdNot)
            return new ApiResponse("This company already added", false);

        Company company = optionalCompany.get();
        Optional<Address> optionalAddress = addressRepository.findById(companyDto.getAddressId());
        if (optionalAddress.isEmpty())
            return new ApiResponse("This address not found", false);

        Address address = optionalAddress.get();

        company.setCorpName(companyDto.getCorpName());
        company.setDirectorName(companyDto.getDirectorName());
        company.setAddress(address);

        companyRepository.save(company);

        return new ApiResponse("The company edited", true);
    }

    public ApiResponse delete(Integer id) {

        Optional<Company> optionalCompany = companyRepository.findById(id);

        if (optionalCompany.isEmpty())
            return new ApiResponse("The company not found", false);

        companyRepository.deleteById(id);
        return new ApiResponse("The company deleted", true);
    }
}
