package uz.pdp.appcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.appcompany.entity.Address;
import uz.pdp.appcompany.payload.ApiResponse;
import uz.pdp.appcompany.repository.AddressRepository;

import java.util.List;
import java.util.Optional;

@Service
public class AddressService {
    @Autowired
    AddressRepository addressRepository;

    public ApiResponse add(Address address) {
        boolean existsByHomeNumberAndStreet = addressRepository.existsByHomeNumberAndStreet(
                address.getHomeNumber(), address.getStreet()
        );
        if (existsByHomeNumberAndStreet)
            return new ApiResponse("This address already added", false);

        addressRepository.save(address);

        return new ApiResponse("The address added", true);
    }

    public List<Address> getAll() {
        List<Address> all = addressRepository.findAll();

        return all;
    }

    public Address getById(Integer id) {
        Optional<Address> optionalCurrency = addressRepository.findById(id);

        return optionalCurrency.orElse(null);
    }

    public ApiResponse edit(Address comingAddress, Integer id) {

        Optional<Address> optionalAddress = addressRepository.findById(id);

        if (optionalAddress.isEmpty())
            return new ApiResponse("The address not found", false);

        boolean existsByHomeNumberAndStreet = addressRepository.existsByHomeNumberAndStreet(comingAddress.getHomeNumber(), comingAddress.getStreet());
        if (existsByHomeNumberAndStreet)
            return new ApiResponse("This address already exist", false);

        Address address = optionalAddress.get();
        address.setStreet(comingAddress.getStreet());
        address.setHomeNumber(comingAddress.getHomeNumber());

        addressRepository.save(address);

        return new ApiResponse("The address edited", true);
    }

    public ApiResponse delete(Integer id) {
        Optional<Address> optionalAddress = addressRepository.findById(id);

        if (optionalAddress.isEmpty())
            return new ApiResponse("The address not found", false);

        addressRepository.deleteById(id);
        return new ApiResponse("The address deleted", true);
    }
}
