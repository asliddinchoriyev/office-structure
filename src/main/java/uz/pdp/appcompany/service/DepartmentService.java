package uz.pdp.appcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.appcompany.entity.Address;
import uz.pdp.appcompany.entity.Company;
import uz.pdp.appcompany.entity.Department;
import uz.pdp.appcompany.payload.ApiResponse;
import uz.pdp.appcompany.payload.CompanyDto;
import uz.pdp.appcompany.payload.DepartmentDto;
import uz.pdp.appcompany.repository.AddressRepository;
import uz.pdp.appcompany.repository.CompanyRepository;
import uz.pdp.appcompany.repository.DepartmentRepository;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {
    @Autowired
    DepartmentRepository departmentRepository;
    @Autowired
    CompanyRepository companyRepository;

    public ApiResponse add(DepartmentDto departmentDto) {

        boolean existsByName = departmentRepository.existsByName(departmentDto.getName());
        if (existsByName)
            return new ApiResponse("This department already added", false);

        Department department = new Department();

        Optional<Company> optionalCompany = companyRepository.findById(departmentDto.getCompanyId());
        if (optionalCompany.isEmpty())
            return new ApiResponse("This company not found", false);

        Company company = optionalCompany.get();

        department.setName(departmentDto.getName());
        department.setCompany(company);

        departmentRepository.save(department);

        return new ApiResponse("The department added", true);
    }

    public List<Department> getAll() {
        List<Department> all = departmentRepository.findAll();

        return all;
    }

    public Department getById(Integer id) {
        Optional<Department> optionalDepartment = departmentRepository.findById(id);

        return optionalDepartment.orElse(null);
    }

    public ApiResponse edit(DepartmentDto departmentDto, Integer id) {

        Optional<Department> optionalDepartment = departmentRepository.findById(id);

        if (optionalDepartment.isEmpty())
            return new ApiResponse("The department not found", false);

        boolean nameAndIdNot = departmentRepository.existsByNameAndIdNot(departmentDto.getName(), id);
        if (nameAndIdNot)
            return new ApiResponse("This department already added", false);


        Optional<Company> optionalCompany = companyRepository.findById(departmentDto.getCompanyId());
        if (optionalCompany.isEmpty())
            return new ApiResponse("This company not found", false);

        Company company = optionalCompany.get();

        Department department = optionalDepartment.get();
        department.setName(departmentDto.getName());
        department.setCompany(company);

        departmentRepository.save(department);

        return new ApiResponse("The department edited", true);
    }

    public ApiResponse delete(Integer id) {

        Optional<Department> optionalDepartment = departmentRepository.findById(id);

        if (optionalDepartment.isEmpty())
            return new ApiResponse("The department not found", false);

        departmentRepository.deleteById(id);
        return new ApiResponse("The department deleted", true);
    }
}
