package uz.pdp.appcompany.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appcompany.entity.Address;
import uz.pdp.appcompany.payload.ApiResponse;
import uz.pdp.appcompany.service.AddressService;

import java.util.List;

@RestController
@RequestMapping(value = "/address")
public class AddressController {
    @Autowired
    AddressService addressService;

    /**
     * @param address
     * @return ResponseEntity<ApiResponse>
     */
    @PostMapping
    public ResponseEntity<ApiResponse> add(@RequestBody Address address) {
        ApiResponse apiResponse = addressService.add(address);

        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }

    /**
     * @return ResponseEntity<List < Address>>
     */

    @GetMapping
    public ResponseEntity<List<Address>> getAll() {
        List<Address> all = addressService.getAll();

        return ResponseEntity.ok(all);
    }

    /**
     * @param id
     * @return ResponseEntity<Address>
     */

    @GetMapping(value = "/{id}")
    public ResponseEntity<Address> getById(@PathVariable Integer id) {
        Address address = addressService.getById(id);

        return ResponseEntity.ok(address);
    }

    /**
     * @param comingAddress
     * @param id
     * @return ResponseEntity<ApiResponse>
     */

    @PutMapping(value = "/{id}")
    public ResponseEntity<ApiResponse> edit(@RequestBody Address comingAddress, @PathVariable Integer id) {
        ApiResponse apiResponse = addressService.edit(comingAddress, id);

        return ResponseEntity.status(apiResponse.isStatus() ? 201 : 409).body(apiResponse);
    }

    /**
     * @param id
     * @return ResponseEntity<ApiResponse>
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<ApiResponse> delete(@PathVariable Integer id) {
        ApiResponse apiResponse = addressService.delete(id);

        return ResponseEntity.status(apiResponse.isStatus() ? 200 : 409).body(apiResponse);
    }
}
